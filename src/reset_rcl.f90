!#####################################################################
!!
!!  File  reset_rcl.f90
!!  is part of RECOLA (REcursive Computation of One Loop Amplitudes)
!!
!!  Copyright (C) 2015-2017   Stefano Actis, Ansgar Denner, 
!!                            Lars Hofer, Jean-Nicolas Lang, 
!!                            Andreas Scharf, Sandro Uccirati
!!
!!  RECOLA is licenced under the GNU GPL version 3, 
!!         see COPYING for details.
!!
!#####################################################################

  module reset_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  use input_rcl
  use amplitude_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  implicit none

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  contains

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine reset_recola_rcl

  ! This subroutine deallocates all global allocatable arrays and sets 
  ! internal variables to the initialization value.
  ! It has to be called at the end of the Recola-session.

  integer :: i,n
  logical :: fileopen

  ! internal_variables
  deallocate (timeTI,timeTC,inpr,legsIn,legsOut,par,hel,resMax,      &
              binRes,parRes,powgs,loop,process)
  if (allocated(momenta))    deallocate(momenta)
  if (allocated(matrix))     deallocate(matrix)
  if (allocated(matrix2))    deallocate(matrix2)
  if (allocated(matrix2h))   deallocate(matrix2h)
  if (allocated(matrix2int)) deallocate(matrix2int)
  if (allocated(matrix2cc))  deallocate(matrix2cc)
  if (allocated(matrix2scc)) deallocate(matrix2scc)
  if (allocated(matrix2sc))  deallocate(matrix2sc)
  deallocate (factor,newleg,oldleg,newbin,oldbin,defp2bin,p2bin,     &
              defresbin,pspbin,mONS,cmONS2,cmREG2,lpmax,cfTot,csTot, &
              pCsTot,csIa,csIq,nIa,pIa,facIa,w0eTot,heli,dualheli,   &
              cd0sMax,w0Tot,w0last,parw0e,binw0e,legw0e,helw0e,      &
              zeroLO,c0EffMax,modaTot,colcoef,colcoefc,comp0gs,      &
              mosm0,binsm0,parsm0,xsm0,gsIncsm0,cosm0,gssm0,cssm0,   &
              dasd0,sesd0,facsd0,gssd0,cssd0,c0TOlp,bm0min,bm0max,   &
              bd0min,bd0max,sbm0,w0inbm0,w0outbm0,winitbm0,typebm0,  &
              sbd0,w0outbd0,winitbd0,gsTot,gs2Tot)
  if (loopMax) deallocate (loopCoef,cEffMax,tiTot,ritiMax,comp1gs,   &
                           mosm1,binsm1,parsm1,gsIncsm1,cosm1,       &
                           rankInsm1,rankOutsm1,ferloopsm1,gssm1,    &
                           cssm1,tism1,dasd1,facsd1,rankOutsd1,      &
                           ferloopsd1,gssd1,cssd1,tisd1,legsti,      &
                           momsti,vmti,rankti,cTOt,cTOfh,cTOih1,     &
                           bm1min,bm1max,sbm1,w1inbm1,w0inbm1,       &
                           w1outbm1,winitbm1,typebm1,bd1min,bd1max,  &
                           sbd1,w1outbd1,winitbd1,w1TotMax,riwMax,   &
                           nCache,nCacheTot,tiCache,CacheOn)

  ! tables
  deallocate (levelLeg,vectorLeg,firstNumber,firstGap,firstNumbers, &
              firstGaps,cm2n,als0R,Qren0R,Nlq0R)
  if (loopMax) deallocate (RtoS,riMin,riMax,ri,RItoR,RItoI,incRI,   &
                           firstRI,dZgs0R)

! Set internal variables to default values

  loopQED  = .true.
  loopWEAK = .true.
  reguScheme = 2
  check_Pole = .false.
  resPar = .false.
  resIR = .false.
  longitudinal = 0
  warnings = 0

  prTot = 0
  loopMax = .false.

! rescaling begin
  ! reset scaling to 1d0 (no scaling)
  call scale_input(1d0/energy_scaling_factor)
  energy_scaling_factor = 1d0
! rescaling end

  ! Set als, Qren, Nfren and Nlq to their initialization values
  als   = als0
  Qren  = Qren0
  Nfren = Nfren0
  Nlq   = Nlq0
  use_active_qmasses = .false.

  ifail = 0

  do i = 1,nOpened
    inquire(file=trim(nameOpened(i)),opened=fileopen,number=n)
    if (fileopen) close(n)
  enddo
  outputfile = 'output.rcl'
  nx = 934758
!  nOpened = 0
!  nameOpened(nOpenedDef) = ''

  processes_generated = .false.
  changed_lambda      = .false.
  changed_DeltaUV     = .false.
  changed_muUV        = .false.
  changed_DeltaIR     = .false.
  changed_muIR        = .false.

  end subroutine reset_recola_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  end module reset_rcl

!#####################################################################



