!#####################################################################
!!
!!  File  process_computation_rcl.f90
!!  is part of RECOLA (REcursive Computation of One Loop Amplitudes)
!!
!!  Copyright (C) 2015-2017   Stefano Actis, Ansgar Denner, 
!!                            Lars Hofer, Jean-Nicolas Lang, 
!!                            Andreas Scharf, Sandro Uccirati
!!
!!  RECOLA is licenced under the GNU GPL version 3, 
!!         see COPYING for details.
!!
!#####################################################################

  module process_computation_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  use input_rcl
  use amplitude_rcl
  use wave_functions_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  implicit none

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  contains

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine set_resonant_squared_momentum_rcl (npr,res,ps)

  ! This subroutine acts on the resonance number "res" for the process 
  ! with process number "npr". It sets the squared momentum of the 
  ! denominator of the resonant propagator to "ps". 
  ! The resonance-number "res" is defined from the process definition 
  ! in the call of define_process_rcl. The first resonant particle 
  ! defined there has "res"=1, the second resonant particle has 
  ! "res=2", and so on. "ps" is a real number.

  integer,  intent(in)  :: npr,res
  real(dp), intent(in)  :: ps

  integer               :: pr,i,l,e0,e1,e2

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of set_resonant_squared_momentum_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*)         'ERROR: set_resonant_squared_momentum_rcl '
      write(nx,'(a,i3)') '        called with undefined process index ',npr
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  l  = legsIn(pr) + legsOut(pr)

  if (res.lt.0.or.res.gt.l) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: set_resonant_squared_momentum_rcl called ', &
                         'with wrong resonance number'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  if (ps.lt.0d0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: set_resonant_squared_momentum_rcl called ', &
                         'with wrong squared momentum'
      write(nx,*)
      write(nx,*)
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  if (.not.resPar(parRes(res,pr))) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: set_resonant_squared_momentum_rcl '
      write(nx,*) '       called for a particle not set as resonant'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  e0 = binRes(res,pr)
  e1 = newbin(e0,pr)
  e2 = 2**l - 1 - e1

  defp2bin(e1,pr) = .true.
     p2bin(e1,pr) = ps

  defp2bin(e2,pr) = .true.
     p2bin(e2,pr) = ps

  end subroutine set_resonant_squared_momentum_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine compute_running_alphas_rcl (Q,Nf,lp)

  ! This subroutine can be called to compute the value for alpha_s at 
  ! the scale "Q" employing the renormalization-group evolution at
  ! "lp" loops ("lp" can take the value "lp=1" or "lp=2").
  ! The integer argument "Nf" selects the flavour scheme according to 
  ! the following rules: 
  ! - "Nf = -1":
  !   The variable flavour scheme is selected, where the number of 
  !   active flavours contributing to the running of alpha_s is set 
  !   to the number of quarks lighter than the scale "Q". 
  ! - "Nf = 3,4,5,6":
  !   The fixed Nf-flavour scheme is selected, where the number of 
  !   active flavours contributing to the running of alpha_s is set 
  !   to "Nf". In this case "Nf" cannot be smaller than the number of 
  !   massless quarks (otherwise the code stops).


  real(dp), intent(in) :: Q
  integer,  intent(in) :: Nf,lp

  integer               :: Nl,Nl0,DNl,aDNl,i,n,k
  real(dp)              :: Q0,ia,b1(6)
  real(dp), allocatable :: bL(:)

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of compute_running_alphas_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  if (Q.le.0d0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) &
        'ERROR: compute_running_alphas_rcl called with wrong scale'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  Nl0 = Nlq0
  select case (Nf)
  case (-1)
    Nl = 0
    do i = 1,6
      if (mq2(i).lt.Q**2) Nl = Nl + 1
    enddo
  case (3,4,5)
    if (mq2(Nf+1).eq.0d0) then
      if (warnings.le.warning_limit) then
        warnings = warnings + 1
        call openOutput
        write(nx,*)
        write(nx,*) 'ERROR: compute_running_alphas_rcl called with a wrong number '
        write(nx,*) '       of light flavours Nf (Nf can not be smaller than '
        write(nx,*) '       the number of massless quarks)'
        write(nx,*)
        call toomanywarnings
      endif
      call istop (ifail,1)
    endif
    Nl = Nf
  case (6)
    Nl = Nf
  case default
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: compute_running_alphas_rcl called with a wrong number of '
      write(nx,*) '       light flavours Nf (accepted values are Nf = -1,3,4,5,6)'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  end select
  DNl = Nl - Nl0
  aDNl = abs(DNl)

  Q0 = Qren0

  allocate ( bL(0:aDNl) )
  select case (DNl)
  case (0)
    bL(0) = beta0(Nl) * log(Q**2/Q0**2) 
  case (1:)
    bL(0) = beta0(Nl0) * log(mq2(Nl0+1)/Q0**2)
    do k = 1,aDNl-1; n = Nl0 + k
      bL(k) = beta0(n) * log(mq2(n+1)/mq2(n))
    enddo
    bL(aDNl) = beta0(Nl) * log(Q**2/mq2(Nl))
  case (:-1)
    bL(0) = beta0(Nl0) * log(mq2(Nl0)/Q0**2)
    do k = 1,aDNl-1; n = Nl0 - k
      bL(k) = beta0(n) * log(mq2(n)/mq2(n+1))
    enddo
    bL(aDNl) = beta0(Nl) * log(Q**2/mq2(Nl+1))
  end select

  ia = pi/als0

  select case (lp)
  case (1)
    ia = ia + sum(bL(0:aDNl))
  case (2)
    do k = 0,aDNl; n = Nl0 + k*sign(1,DNl)
      ia = ia + bL(k) + rb1(n) * log( 1d0 + bL(k)/( ia + rb1(n) ) )
    enddo
  case default
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: compute_running_alphas_rcl called with a wrong number of '
      write(nx,*) '       loops lp for the running (accepted values are lp = 1,2)'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  end select

  als   = pi/ia
  Qren  = Q
  Nfren = Nf
  Nlq   = Nl

  end subroutine compute_running_alphas_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine compute_process_rcl (npr,p,order,A2,momenta_check)

  ! This subroutine computes the structure-dressed helicity 
  ! amplitudes and the summed squared amplitude for the process with 
  ! process number "npr". 
  ! "p" are the external momenta of the process.
  ! "order" is the loop order. Accepted values are 'LO' and 'NLO'.
  ! "A2(0)" is the summed squared  LO amplitude.
  ! "A2(1)" is the summed squared NLO amplitude.
  ! Summed squared amplitude means:
  ! - summed over all computed powers of g_s
  ! - summed over colour and spins for outgoing particles and averaged 
  !   for the incoming particles. 
  ! If some particles were defined as polarized, no sum/average is 
  ! performed on these particles. 
  ! "momenta_check" tells whether the phase-space point is good 
  ! (momenta_check=.true.) or bad (momenta_check=.false).

  integer,          intent(in)            :: npr
  real(dp),         intent(in)            :: p(0:,:)
  character(len=*), intent(in)            :: order
  real(dp),         intent(out), optional :: A2(0:1)
  logical,          intent(out), optional :: momenta_check

  integer  :: pr,i,legs,long_tmp


  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of compute_process_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,'(a,i3)') ' ERROR: compute_process_rcl called '// &
                                 'with undefined process index ',npr
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)

  if (size(p,2).ne.legs.or.size(p,1).ne.4) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: compute_process_rcl called with wrong momenta'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  if (order.ne.'LO'.and.order.ne.'NLO') then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: compute_process_rcl called at the wrong '// &
                         'loop order '
      write(nx,*) "       (accepted values are order = 'LO','NLO')"
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  call set_momenta (pr,p)
  if (present(momenta_check)) momenta_check = momcheck

  if (writeMat+writeMat2.ge.1) then
    call print_process_and_momenta (pr)
    call print_rescaling
    call print_parameters_change
  endif

  if (order .eq. 'NLO' .and. longitudinal_nlo .and. longitudinal .ne. 0) then
  ! If longitudinal and longitudinal_nlo is selected, the QED Ward identity
  ! is only applied to the one-loop matrix element and the LO matrix 
  ! element is recomputed with the full polarization dependence.
    call compute_amplitude (pr,'NLO')
    long_tmp = longitudinal
    longitudinal = 0
    call compute_amplitude (pr,'LO')
    longitudinal = long_tmp
  else
    call compute_amplitude (pr,order)
  end if

  call rescale_amplitude (pr,order)

  if (writeMat.ge.1) call print_amplitude (pr,order)

  call compute_squared_amplitude (pr,order)

  if (writeMat2.ge.1) call print_squared_amplitude (pr,order)

  if (present(A2)) then
    A2(0) = sum(matrix2(0:gs2Tot(0,pr),0,pr))
    A2(1) = sum(matrix2(0:gs2Tot(1,pr),4,pr))
  endif

  end subroutine compute_process_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine rescale_process_rcl (npr,order,A2)

  ! This subroutine adjusts the results calculated by 
  ! compute_process_rcl for a new value of alpha_s, rescaling the 
  ! structure-dressed helicity amplitudes and recomputing the summed 
  ! squared amplitude for the process with process number "npr". 
  ! "order" and "A2" are the same has for compute_process_rcl.

  integer,          intent(in)            :: npr
  character(len=*), intent(in)            :: order
  real(dp),         intent(out), optional :: A2(0:1)

  integer     :: pr,i

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of rescale_process_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,'(a,i3)') ' ERROR: rescale_process_rcl called '// &
                                 'with undefined process index ',npr
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  if (order.ne.'LO'.and.order.ne.'NLO') then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: rescale_process_rcl called at the wrong '// &
                         'loop order '
      write(nx,*) "       (accepted values are order = 'LO','NLO')"
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  if (writeMat+writeMat2.ge.1) call print_rescaling
  if (writeMat+writeMat2.ge.1) call print_parameters_change

  call rescale_amplitude (pr,order)

  if (writeMat.ge.1) call print_amplitude (pr,order)

  call compute_squared_amplitude (pr,order)

  if (writeMat2.ge.1) call print_squared_amplitude (pr,order)

  if (present(A2)) then
    A2(0) = sum(matrix2(0:gs2Tot(0,pr),0,pr))
    A2(1) = sum(matrix2(0:gs2Tot(1,pr),4,pr))
  endif

  end subroutine rescale_process_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine get_colour_configurations_rcl (npr,cols)

  ! This subroutine extracts all colour configurations of the 
  ! process with process number "npr" and write it in the output 
  ! variable "cols(1:l,1:csTot)" (csTot is the total number of 
  ! colour configuations of the process "npr" and l is its total 
  ! number of external legs).
  ! "cols(1:l,n)" describes the n^th colour structure and is a vector 
  ! of type integer and length l, where each position in the vector
  ! corresponds to one of the l external particles of process "npr" 
  ! (ordered as in the process definition).
  ! For colourless particles, incoming quarks and outgoing 
  ! anti-quarks, the corresponding entry in "cols" is 0.
  ! For all other particles (gluons, outgoing quarks and incoming 
  ! anti-quarks), the entries are permutations, without repetition, 
  ! of the positions of the gluons, incoming quarks or outgoing 
  ! anti-quarks in the process definition. 
  ! "cols(1:l,n)" can be used as the input vaiable "colour" in the 
  ! subroutine get_amplitude_rcl.

  integer, intent(in)               :: npr
  integer, intent(out), allocatable :: cols(:,:)

  integer :: pr,i,j,legs

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of get_colour_configurations_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      if (warnings.eq.warning_limit+1) then
        write(nx,*)
        write(nx,*) ' Too many WARNINGs for this run:'
        write(nx,*) ' Further WARNINGs will not be printed'
        write(nx,*)
      endif
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,'(a,i3)') ' ERROR: get_colour_configurations_rcl called '// &
                                 'with undefined process index ',npr
      write(nx,*)
      if (warnings.eq.warning_limit+1) then
        write(nx,*)
        write(nx,*) ' Too many WARNINGs for this run:'
        write(nx,*) ' Further WARNINGs will not be printed'
        write(nx,*)
      endif
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)

  allocate (cols(1:legs,1:csTot(pr)))

  do i = 1, csTot(pr)
    do j = 1,legs
      if (csIq(j,i,pr).ne.0) then
        cols(oldleg(j,pr),i) = oldleg(csIq(j,i,pr),pr)
      else
        cols(oldleg(j,pr),i) = 0
      endif
    enddo
  enddo

  end subroutine get_colour_configurations_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine get_helicity_configurations_rcl (npr,hels)

  ! This subroutine extracts all helicity configurations of the 
  ! process with process number "npr" and write it in the output 
  ! variable "hels(1:l,1:cfTot)" (cfTot is the total number of 
  ! helicity configuations of the process "npr" and l is its total 
  ! number of external legs).
  ! "hels(1:l,n)" describes the n^th helicity configuration and is a 
  ! vector of type integer and length l, where each position in the 
  ! vectorcorresponds to one of the l external particles of process 
  ! "npr" (ordered as in the process definition).
  ! Its entries are the helicities of the particles:
  !   left-handed  fermions/antifermions -> -1
  !   right-handed fermions/antifermions -> +1
  !   logitudinal vector bosons          ->  0
  !   transverse vector bosons           -> -1, +1
  !   scalar particles                   ->  0
  ! Example:
  !   Process 'u g -> W+ d'
  !   "hels(1:4,n)" = (/-1,+1,-1,-1/) means
  !   left-handed up-quark
  !   transverse gluon (helicity = +1)
  !   transverse W+ (helicity = -1)
  !   left-handed down-quark
  ! "hels(1:l,n)" can be used as the input vaiable "hel" in the 
  ! subroutines get_amplitude_rcl and 
  ! get_polarized_squared_amplitude_rcl.

  integer, intent(in)               :: npr
  integer, intent(out), allocatable :: hels(:,:)

  integer :: pr,i,j,legs

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of get_helicity_configurations_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      if (warnings.eq.warning_limit+1) then
        write(nx,*)
        write(nx,*) ' Too many WARNINGs for this run:'
        write(nx,*) ' Further WARNINGs will not be printed'
        write(nx,*)
      endif
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,'(a,i3)') ' ERROR: get_helicity_configurations_rcl called '// &
                                 'with undefined process index ',npr
      write(nx,*)
      if (warnings.eq.warning_limit+1) then
        write(nx,*)
        write(nx,*) ' Too many WARNINGs for this run:'
        write(nx,*) ' Further WARNINGs will not be printed'
        write(nx,*)
      endif
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)

  allocate (hels(1:legs,1:cfTot(pr)))

  do i = 1, cfTot(pr)
    do j = 1,legsIn(pr)
      hels(j,i) = heli(newleg(j,pr),i,pr)
      hels(j,i) = heli(newleg(j,pr),i,pr)
    enddo
    do j = legsIn(pr)+1,legs
      hels(j,i) = - heli(newleg(j,pr),i,pr)
    enddo
  enddo

  end subroutine get_helicity_configurations_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine get_amplitude_rcl (npr,pow,order,colour,hel,A)

  ! This subroutine extracts a specific contribution to the amplitude 
  ! of the process with process number "npr", according to the values 
  ! of "pow", "order", "colour" and "hel".
  ! - "pow" is the power of alpha_s of the contribution.
  ! - "order" is the loop-order of the contribution. 
  !   It is a character variable with accepted values:
  !   'LO'     -> tree squared amplitude
  !   'NLO'    -> loop squared amplitude
  !   'NLO-D4' -> loop squared amplitude, bare loop contribution
  !   'NLO-CT' -> loop squared amplitude, counterterms contribution
  !   'NLO-R2' -> loop squared amplitude, rational terms 
  !               contribution 
  ! - "colour(1:l)" describes the colour structure and is a vector of 
  !   type integer and length l, where each position in the vector
  !   corresponds to one of the l external particles of process "npr" 
  !   (ordered as in the process definition).
  !   For colourless particles, incoming quarks and outgoing 
  !   anti-quarks, the corresponding entry in "colour" must be 0.
  !   For all other particles (gluons, outgoing quarks and incoming 
  !   anti-quarks), the entries must be, without repetition, the 
  !   positions of the gluons, incoming quarks or outgoing 
  !   anti-quarks in the process definition. 
  ! - "hel" is the helicity of the contribution.
  !   It is a vector whose entries are the helicities of the 
  !   particles:
  !   left-handed  fermions/antifermions -> -1
  !   right-handed fermions/antifermions -> +1
  !   logitudinal vector bosons          ->  0
  !   transverse vector bosons           -> -1, +1
  !   scalar particles                   ->  0
  !   Example:
  !   Process 'u g -> W+ d'
  !   "hel" = (/-1,+1,-1,-1/) means
  !   left-handed up-quark
  !   transverse gluon (helicity = +1)
  !   transverse W+ (helicity = -1)
  !   left-handed down-quark

  ! This routine must be called after compute_process_rcl or 
  ! rescale_process_rcl for the process with process number "npr". 

  integer,              intent(in)  :: npr,pow,colour(1:),hel(1:)
  character(len=*),     intent(in)  :: order
  complex(dp),          intent(out) :: A

  integer               :: pr,i,j,h,o,legs,cs,kf
  integer, allocatable  :: helk(:),cs0(:)
  character(2)          :: t,t2,t2c
  character(8)          :: c

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of get_amplitude_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,'(a,i3)') ' ERROR: get_amplitude_rcl called '// &
                                 'with undefined process index ',npr
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)

  if ( size(colour,1).ne.legs) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: get_amplitude_rcl called with a wrong '// &
                         'colour vector (the number of '
      write(nx,*) '       entries must coincide with the number of '// &
                         'legs of the process)'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif
  do i = 1,legs
    if ( colour(i).lt.0.or.colour(i).gt.legs ) then
      if (warnings.le.warning_limit) then
        warnings = warnings + 1
        call openOutput
        write(nx,*)
        write(nx,*) 'ERROR: get_amplitude_rcl called with a wrong '// &
                           'colour vector (the entries can not '
        write(nx,*) '       be negative or bigger than the number '// &
                           'of legs of the process)'
        write(nx,*)
        call toomanywarnings
      endif
      call istop (ifail,1)
    endif
    do j = 1,i-1
      if ( colour(i).eq.colour(j).and.colour(i).ne.0) then
        if (warnings.le.warning_limit) then
          warnings = warnings + 1
          call openOutput
          write(nx,*)
          write(nx,*) 'ERROR: get_amplitude_rcl called with a wrong '// &
                             'colour vector (the non-zero entries '// &
                             'must differ)'
          write(nx,*)
          call toomanywarnings
        endif
        call istop (ifail,1)
      endif
    enddo
  enddo
  do i = 1,legs
    t2  = cftype2(par(i,pr))
    if (colour(i).ne.0) then; t2c = cftype2(par(colour(i),pr))
    else;                     t2c = '0'
    endif
    if ( ((t2c.ne.'g'.and.t2c.ne.'q' ).and.colour(i).ne.0) .or. &
         ((t2 .eq.'g'.or. t2 .eq.'q~').and.colour(i).eq.0) .or. &
         ((t2 .ne.'g'.and.t2 .ne.'q~').and.colour(i).ne.0)      &
       ) then
      if (warnings.le.warning_limit) then
        warnings = warnings + 1
        call openOutput
        write(nx,*)
        write(nx,*) 'ERROR: get_amplitude_rcl called with a wrong '// &
                           'colour vector'
        write(nx,*)
        call toomanywarnings
      endif
      call istop (ifail,1)
    endif
  enddo

  allocate (cs0(legs)); cs0 = 0
  do i = 1,legs
    if (colour(i).ne.0) cs0(newleg(i,pr)) = newleg(colour(i),pr)
  enddo
  cs = 0
  do i = 1,csTot(pr)
    if ( sum(abs(csIq(1:legs,i,pr)-cs0(1:legs))).eq.0 ) then
      cs = i
      exit
    endif
  enddo
  deallocate (cs0)

  if (size(hel).ne.legs) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: get_amplitude_rcl called with a wrong '// &
                         'helicity vector (the number of '
      write(nx,*) '       entries must coincide with the number of '// &
                         'legs of the process)'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif
  do i = 1,legs
    h = hel(i)
    c = cpar(par(i,pr))
    t = cftype(par(i,pr))
    if ( (h.lt.-1)              .or. &
         (h.gt.+1)              .or. &
         (c.eq.'g' .and.h.eq.0) .or. &
         (c.eq.'A' .and.h.eq.0) .or. &
         (t.eq.'f' .and.h.eq.0) .or. &
         (t.eq.'f~'.and.h.eq.0) .or. &
         (t.eq.'s' .and.h.ne.0)      &
       ) then
      if (warnings.le.warning_limit) then
        warnings = warnings + 1
        call openOutput
        write(nx,*)
        write(nx,*) 'ERROR: get_amplitude_rcl called with a wrong '// &
                           'helicity vector'
        write(nx,*)
        call toomanywarnings
      endif
      call istop (ifail,1)
    endif
  enddo

  allocate (helk(legs))
  do i = 1,legsIn(pr)
    helk(newleg(i,pr)) = hel(i)
  enddo
  do i = legsIn(pr)+1,legs
    helk(newleg(i,pr)) = - hel(i)
  enddo
  kf = 0
  do i = 1,cfTot(pr)
    if ( sum(abs( heli(1:legs,i,pr) - helk(1:legs) )).eq.0 ) then
      kf = i
      exit
    endif
  enddo

  if     (order.eq.'LO'    ) then; o = 0
  elseif (order.eq.'NLO-D4') then; o = 1
  elseif (order.eq.'NLO-CT') then; o = 2
  elseif (order.eq.'NLO-R2') then; o = 3
  elseif (order.eq.'NLO'   ) then; o = 4
  else
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: get_amplitude_rcl called at the '// &
                         'wrong loop order (accepted '
      write(nx,*) "       values are order = 'LO','NLO','NLO-D4',"// &
                         "'NLO-CT','NLO-R2')"
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  if ((o.eq.0.and.(pow.lt.0.or.pow.gt.legs-2)) .or. &
      (o.gt.0.and.(pow.lt.0.or.pow.gt.legs  ))      &
     ) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: get_amplitude_rcl called with wrong gs power'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  A = c0d0
  if (cs.ne.0.and.kf.ne.0) A = matrix(cs,pow,kf,o,pr)

  end subroutine get_amplitude_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine get_squared_amplitude_rcl (npr,pow,order,A2)

  ! This subroutine extracts the computed value of the summed squared 
  ! amplitude with "pow" power of alpha_s at loop-order "order" for 
  ! the process with process number "npr".
  ! "order: is a character variable with accepted values:
  ! 'LO'     -> tree squared amplitude
  ! 'NLO'    -> loop squared amplitude
  ! 'NLO-D4' -> loop squared amplitude, bare loop contribution
  ! 'NLO-CT' -> loop squared amplitude, counterterms contribution
  ! 'NLO-R2' -> loop squared amplitude, rational terms contribution 

  ! This routine must be called after compute_process_rcl or 
  ! rescale_process_rcl for the process with process number "npr". 

  integer,          intent(in)  :: npr,pow
  character(len=*), intent(in)  :: order
  real(dp),         intent(out) :: A2

  integer :: pr,i,legs,gspower,o

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of get_squared_amplitude_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,'(a,i3)') ' ERROR: get_squared_amplitude_rcl called '// &
                                 'with undefined process index ',npr
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)

  if     (order.eq.'LO'    ) then; o = 0
  elseif (order.eq.'NLO-D4') then; o = 1
  elseif (order.eq.'NLO-CT') then; o = 2
  elseif (order.eq.'NLO-R2') then; o = 3
  elseif (order.eq.'NLO'   ) then; o = 4
  else
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: get_squared_amplitude_rcl called at '// &
                         'the wrong loop order (accepted '
      write(nx,*) "       values are order = 'LO','NLO','NLO-D4',"// &
                         "'NLO-CT','NLO-R2')"
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  if ((pow.lt.0)                                       .or. &
      (o.eq.0.and.                      pow.gt.legs-2) .or. &
      (o.gt.0.and.(.not.zeroLO(pr)).and.pow.gt.legs-1) .or. &
      (o.gt.0.and.      zeroLO(pr) .and.pow.gt.legs  )      &
     ) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: get_squared_amplitude_rcl called', &
                        ' with wrong alphas power'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  gspower = 2*pow

  A2 = matrix2(gspower,o,pr)

  end subroutine get_squared_amplitude_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine get_polarized_squared_amplitude_rcl (npr,pow,order,hel,A2h)

  ! This subroutine extracts the computed value of the squared 
  ! amplitude summed over colour with polarization "hel", with "pow" 
  ! power of alpha_s at loop-order "order" for the process with 
  ! process number "npr".
  ! - "pow" is the power of alpha_s of the contribution.
  ! - "order" is the loop-order of the contribution. 
  !   It is a character variable with accepted values:
  !   'LO'     -> tree squared amplitude
  !   'NLO'    -> loop squared amplitude
  !   'NLO-D4' -> loop squared amplitude, bare loop contribution
  !   'NLO-CT' -> loop squared amplitude, counterterms contribution
  !   'NLO-R2' -> loop squared amplitude, rational terms 
  !                 contribution 
  ! - "hel" is the helicity of the contribution.
  !   It is a vector whose entries are the helicities of the 
  !   particles:
  !   left-handed  fermions/antifermions -> -1
  !   right-handed fermions/antifermions -> +1
  !   logitudinal vector bosons          ->  0
  !   transverse vector bosons           -> -1, +1
  !   scalar particles                   ->  0
  !   Example:
  !   Process 'u g -> W+ d'
  !   "hel" = (/-1,+1,-1,-1/) means
  !   left-handed up-quark
  !   transverse gluon (helicity = +1)
  !   transverse W+ (helicity = -1)
  !   left-handed down-quark

  ! This routine must be called after compute_process_rcl or 
  ! rescale_process_rcl for the process with process number "npr". 

  integer,          intent(in)  :: npr,pow,hel(1:)
  character(len=*), intent(in)  :: order
  real(dp),         intent(out) :: A2h

  integer               :: pr,i,legs,gspower,kf,h,o
  character(2)          :: t
  character(8)          :: c
  integer, allocatable  :: helk(:)

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of get_polarized_squared_amplitude_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*)         'ERROR: get_polarized_squared_amplitude_rcl '
      write(nx,'(a,i3)') '        called with undefined process index ',npr
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)

  if (size(hel).ne.legs) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: get_polarized_squared_amplitude_rcl called '// &
                         'with a wrong '
      write(nx,*) '       helicity vector (the number of entries must '// &
                         'coincide '
      write(nx,*) '       with the number of legs of the process)'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif
  do i = 1,legs
    h = hel(i)
    c = cpar(par(i,pr))
    t = cftype(par(i,pr))
    if ( (h.lt.-1)              .or. &
         (h.gt.+1)              .or. &
         (c.eq.'g' .and.h.eq.0) .or. &
         (c.eq.'A' .and.h.eq.0) .or. &
         (t.eq.'f' .and.h.eq.0) .or. &
         (t.eq.'f~'.and.h.eq.0) .or. &
         (t.eq.'s' .and.h.ne.0)      &
       ) then
      if (warnings.le.warning_limit) then
        warnings = warnings + 1
        call openOutput
        write(nx,*)
        write(nx,*) 'ERROR: get_polarized_squared_amplitude_rcl called '// &
                           'with a wrong helicity vector'
        write(nx,*)
        call toomanywarnings
      endif
      call istop (ifail,1)
    endif
  enddo

  allocate (helk(legs))
  do i = 1,legsIn(pr)
    helk(newleg(i,pr)) = hel(i)
  enddo
  do i = legsIn(pr)+1,legs
    helk(newleg(i,pr)) = - hel(i)
  enddo
  kf = 0
  do i = 1,cfTot(pr)
    if ( sum(abs( heli(:,i,pr) - helk(:) )).eq.0 ) then
      kf = i
      exit
    endif
  enddo

  if     (order.eq.'LO'    ) then; o = 0
  elseif (order.eq.'NLO-D4') then; o = 1
  elseif (order.eq.'NLO-CT') then; o = 2
  elseif (order.eq.'NLO-R2') then; o = 3
  elseif (order.eq.'NLO'   ) then; o = 4
  else
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) "ERROR: get_polarized_squared_amplitude_rcl called "
      write(nx,*) "       at the wrong loop order (accepted values are "
      write(nx,*) "       order = 'LO','NLO','NLO-D4','NLO-CT','NLO-R2')"
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  if ((pow.lt.0)                                       .or. &
      (o.eq.0.and.                      pow.gt.legs-2) .or. &
      (o.gt.0.and.(.not.zeroLO(pr)).and.pow.gt.legs-1) .or. &
      (o.gt.0.and.      zeroLO(pr) .and.pow.gt.legs  )      &
     ) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: get_polarized_squared_amplitude_rcl', &
                        ' called with wrong alphas power'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  gspower = 2*pow

  A2h = 0d0
  if (kf.ne.0) A2h = matrix2h(gspower,kf,o,pr)

  end subroutine get_polarized_squared_amplitude_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine compute_colour_correlation_rcl (npr,p,i1,i2,A2cc, &
                                             momenta_check)

  ! This subroutine computes the colour-correlated summed squared 
  ! amplitude, between particle with leg number "i1" and particle 
  ! with leg number "i2", for the process with process number "npr". 
  ! "p" are the external momenta of the process.
  ! "A2cc" is the colour-correlated summed squared  LO 
  ! amplitude.
  ! Summed squared amplitude means:
  ! - summed over all computed powers of g_s
  ! - summed over colour and spins for outgoing particles and averaged 
  !   for the incoming particles. 
  ! If some particles were defined as polarized, no sum/average is 
  ! performed on these particles. 
  ! "momenta_check" tells whether the phase-space point is good 
  ! (momenta_check=.true.) or bad (momenta_check=.false).

  integer,  intent(in)            :: npr,i1,i2
  real(dp), intent(in)            :: p(0:,:)
  real(dp), intent(out), optional :: A2cc
  logical,  intent(out), optional :: momenta_check

  integer :: pr,i,legs,j1,j2

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of compute_colour_correlation_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*)         'ERROR: compute_colour_correlation_rcl called '
      write(nx,'(a,i3)') '        with undefined process index ',npr
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)

  if ((i1.le.0.or.i1.gt.legs).or.(i2.le.0.or.i2.gt.legs)) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: compute_colour_correlation_rcl called ', &
                         'with wrong indices'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  if (size(p,2).ne.legs.or.size(p,1).ne.4) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: compute_colour_correlation_rcl called ', &
                         'with wrong momenta'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  call set_momenta (pr,p)
  if (present(momenta_check)) momenta_check = momcheck

  if (writeCor.ge.1) then
    call print_process_and_momenta (pr)
    call print_rescaling
    call print_parameters_change
  endif

  call compute_amplitude (pr,'LO')

  call rescale_amplitude (pr,'LO')

  if (writeMat.ge.1) call print_amplitude (pr,'LO')

  call compute_squared_amplitude_cc (pr,i1,i2)

  if (writeCor.ge.1) then
    call print_squared_amplitude_cc (pr,i1,i2)
    write(nx,'(1x,75("x"))')
    write(nx,*)
    write(nx,*)
  endif

  if (present(A2cc)) then
    j1 = newleg(i1,pr)
    j2 = newleg(i2,pr)
    A2cc = sum(matrix2cc(0:gs2Tot(0,pr),j1,j2,pr))
  endif

  end subroutine compute_colour_correlation_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine compute_all_colour_correlations_rcl (npr,p,momenta_check)

  ! This subroutine computes the colour-correlated summed squared 
  ! amplitudes for the process with process number "npr".
  ! "p" are the external momenta of the process.
  ! The colour correlation is done for all possible pairs coloured
  ! particles.
  ! Summed squared amplitude means:
  ! - summed over all computed powers of g_s
  ! - summed over colour and spins for outgoing particles and averaged 
  !   for the incoming particles. 
  ! If some particles were defined as polarized, no sum/average is 
  ! performed on these particles. 
  ! "momenta_check" tells whether the phase-space point is good 
  ! (momenta_check=.true.) or bad (momenta_check=.false).

  integer,  intent(in)            :: npr
  real(dp), intent(in)            :: p(0:,:)
  logical,  intent(out), optional :: momenta_check

  integer :: pr,i,legs,i1,i2

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of compute_all_colour_correlations_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*)         'ERROR: compute_all_colour_correlations_rcl '
      write(nx,'(a,i3)') '        called with undefined process index ',npr
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)

  if (size(p,2).ne.legs.or.size(p,1).ne.4) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: compute_all_colour_correlations_rcl called ', &
                         'with wrong momenta'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  call set_momenta (pr,p)
  if (present(momenta_check)) momenta_check = momcheck

  if (writeCor.ge.1) then
    call print_process_and_momenta (pr)
    call print_rescaling
    call print_parameters_change
  endif

  call compute_amplitude (pr,'LO')

  call rescale_amplitude (pr,'LO')

  if (writeMat.ge.1) call print_amplitude (pr,'LO')

  do i1 = 1,legs
  do i2 = 1,legs
    call compute_squared_amplitude_cc (pr,i1,i2)
    if (writeCor.ge.1) call print_squared_amplitude_cc (pr,i1,i2)
  enddo
  enddo

  if (writeCor.ge.1) then
    call openOutput
    write(nx,'(1x,75("x"))')
    write(nx,*)
    write(nx,*)
  endif

  end subroutine compute_all_colour_correlations_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine rescale_colour_correlation_rcl (npr,i1,i2,A2cc)

  ! This subroutine adjusts the results calculated by 
  ! compute_colour_correlation_rcl for a new value of alpha_s, 
  ! rescaling the structure-dressed helicity amplitudes and 
  ! recomputing the colour-correlated summed squared amplitude for 
  ! the process with process number "npr". 
  ! "i1", "i2" and "A2cc" are the same has for 
  ! compute_colour_correlation_rcl.

  integer,  intent(in)            :: npr,i1,i2
  real(dp), intent(out), optional :: A2cc

  integer :: pr,i,legs,j1,j2

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of rescale_colour_correlation_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*)         'ERROR: rescale_colour_correlation_rcl called '
      write(nx,'(a,i3)') '        with undefined process index ',npr
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)

  if ((i1.le.0.or.i1.gt.legs).or.(i2.le.0.or.i2.gt.legs)) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: rescale_colour_correlation_rcl called ', &
                         'with wrong indices'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  if (writeCor.ge.1) call print_rescaling
  if (writeCor.ge.1) call print_parameters_change

  call rescale_amplitude (pr,'LO')

  if (writeMat.ge.1) call print_amplitude (pr,'LO')

  call compute_squared_amplitude_cc (pr,i1,i2)

  if (writeCor.ge.1) then
    call print_squared_amplitude_cc (pr,i1,i2)
    write(nx,'(1x,75("x"))')
    write(nx,*)
    write(nx,*)
  endif

  if (present(A2cc)) then
    j1 = newleg(i1,pr)
    j2 = newleg(i2,pr)
    A2cc = sum(matrix2cc(0:gs2Tot(0,pr),j1,j2,pr))
  endif

  end subroutine rescale_colour_correlation_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine rescale_all_colour_correlations_rcl (npr)

  ! This subroutine adjusts the results calculated by 
  ! compute_all_colour_correlations_rcl for a new value of alpha_s, 
  ! rescaling the structure-dressed helicity amplitudes and 
  ! recomputing the colour-correlated summed squared amplitude for 
  ! the process with process number "npr". 

  integer,  intent(in) :: npr

  integer :: pr,i,legs,i1,i2

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of rescale_all_colour_correlations_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*)         'ERROR: rescale_all_colour_correlations_rcl '
      write(nx,'(a,i3)') '        called with undefined process index ',npr
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)

  if (writeCor.ge.1) call print_rescaling
  if (writeCor.ge.1) call print_parameters_change

  call rescale_amplitude (pr,'LO')

  if (writeMat.ge.1) call print_amplitude (pr,'LO')

  do i1 = 1,legs
  do i2 = 1,legs
    call compute_squared_amplitude_cc (pr,i1,i2)
    if (writeCor.ge.1) call print_squared_amplitude_cc (pr,i1,i2)
  enddo
  enddo

  if (writeCor.ge.1) then
    call openOutput
    write(nx,'(1x,75("x"))')
    write(nx,*)
    write(nx,*)
  endif

  end subroutine rescale_all_colour_correlations_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine get_colour_correlation_rcl (npr,pow,i1,i2,A2cc)

  ! This subroutine extracts the computed value of the 
  ! colour-correlated summed squared amplitude, with "pow" power of 
  ! alpha_s, between particle with leg number "i1" and particle with 
  ! leg number "i2", for the process with process number "npr".
  ! This routine must be called after compute_colour_correlation_rcl 
  ! or rescale_colour_correlation_rcl (for process with process number 
  ! "npr" for particles "i1" and "i2") or after 
  ! compute_all_colour_correlations_rcl for process or
  ! rescale_all_colour_correlations_rcl for process with process 
  ! number "npr".

  integer,  intent(in)  :: npr,pow,i1,i2
  real(dp), intent(out) :: A2cc

  integer :: pr,i,legs,j1,j2,gspower

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of get_colour_correlation_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,'(a,i3)') ' ERROR: get_colour_correlation_rcl called '// &
                                 'with undefined process index ',npr
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)

  if ((i1.le.0.or.i1.gt.legs).or.(i2.le.0.or.i2.gt.legs)) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: get_colour_correlation_rcl called ', &
                         'with wrong indices'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  j1 = newleg(i1,pr)
  j2 = newleg(i2,pr)

  if (pow.lt.0.or.pow.gt.legs-2) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: get_colour_correlation_rcl called ', &
                         'with wrong alphas power'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  gspower = 2*pow

  A2cc = matrix2cc(gspower,j1,j2,pr)

  end subroutine get_colour_correlation_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine compute_spin_colour_correlation_rcl (npr,p,i1,i2,v, &
                                                  A2scc,momenta_check)

  ! This subroutine computes the colour-correlated summed squared 
  ! amplitude, between particle with leg number "i1" and particle 
  ! with leg number "i2", for the process with process number "npr", 
  ! allowing spin-correlation if particle with leg number "i1"
  ! is a gluon.
  ! The spin-correlation is achieved by a user-defined polarization 
  ! vector ("v") for the particle with leg number "i1", which 
  ! substitutes the usual one. 
  ! "p" are the external momenta of the process.
  ! "A2scc" is the spin-colour-correlated summed squared  LO 
  ! amplitude.
  ! Summed squared amplitude means:
  ! - summed over all computed powers of g_s
  ! - summed over colour and spins for outgoing particles and averaged 
  !   for the incoming particles. 
  ! If some particles were defined as polarized, no sum/average is 
  ! performed on these particles. 
  ! "momenta_check" tells whether the phase-space point is good 
  ! (momenta_check=.true.) or bad (momenta_check=.false).

  integer,     intent(in)            :: npr,i1,i2
  real(dp),    intent(in)            :: p(0:,:)
  complex(dp), intent(in)            :: v(0:)
  real(dp),    intent(out), optional :: A2scc
  logical,     intent(out), optional :: momenta_check

  integer :: pr,i,legs,j1,j2

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of compute_spin_colour_correlation_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*)         'ERROR: compute_spin_colour_correlation_rcl '
      write(nx,'(a,i3)') '        called with undefined process index ',npr
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)

  if ((cpar(par(i1,pr)).ne.'g') .or. &
      (i1.le.0.or.i1.gt.legs)   .or. &
      (i2.le.0.or.i2.gt.legs)        &
    ) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: compute_spin_colour_correlation_rcl called ', &
                         'with wrong indices'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  if (size(p,2).ne.legs.or.size(p,1).ne.4) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: compute_spin_colour_correlation_rcl called ', &
                         'with wrong momenta'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  call set_momenta (pr,p)
  if (present(momenta_check)) momenta_check = momcheck

  if (writeCor.ge.1) then
    call print_process_and_momenta (pr)
    call print_rescaling
    call print_parameters_change
  endif

  call compute_amplitude (pr,'LO')

  call rescale_amplitude (pr,'LO')

  if (writeMat.ge.1) call print_amplitude (pr,'LO')

  call compute_squared_amplitude_scc (pr,i1,i2,v)

  if (writeCor.ge.1) call print_squared_amplitude_scc (pr,i1,i2,v)

  if (present(A2scc)) then
    j1 = newleg(i1,pr)
    j2 = newleg(i2,pr)
    A2scc = sum(matrix2scc(0:gs2Tot(0,pr),j1,j2,pr))
  endif

  end subroutine compute_spin_colour_correlation_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine rescale_spin_colour_correlation_rcl (npr,i1,i2,v,A2scc)

  ! This subroutine adjusts the results calculated by 
  ! compute_spin_colour_correlation_rcl for a new value of alpha_s, 
  ! rescaling the structure-dressed helicity amplitudes and 
  ! recomputing the spin-colour-correlated summed squared amplitude 
  ! for the process with process number "npr". 
  ! "i1", "i2", "v" and "A2scc" are the same has for 
  ! compute_spin_colour_correlation_rcl.

  integer,     intent(in)            :: npr,i1,i2
  complex(dp), intent(in)            :: v(0:)
  real(dp),    intent(out), optional :: A2scc

  integer :: pr,i,legs,j1,j2

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of rescale_spin_colour_correlation_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*)         'ERROR: rescale_spin_colour_correlation_rcl '
      write(nx,'(a,i3)') '        called with undefined process index ',npr
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)

  if ((cpar(par(i1,pr)).ne.'g') .or. &
      (i1.le.0.or.i1.gt.legs)   .or. &
      (i2.le.0.or.i2.gt.legs)        &
    ) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: rescale_spin_colour_correlation_rcl called ', &
                         'with wrong indices'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  if (writeCor.ge.1) call print_rescaling
  if (writeCor.ge.1) call print_parameters_change

  call rescale_amplitude (pr,'LO')

  if (writeMat.ge.1) call print_amplitude (pr,'LO')

  call compute_squared_amplitude_scc (pr,i1,i2,v)

  if (writeCor.ge.1) call print_squared_amplitude_scc (pr,i1,i2,v)

  if (present(A2scc)) then
    j1 = newleg(i1,pr)
    j2 = newleg(i2,pr)
    A2scc = sum(matrix2scc(0:gs2Tot(0,pr),j1,j2,pr))
  endif

  end subroutine rescale_spin_colour_correlation_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine get_spin_colour_correlation_rcl (npr,pow,i1,i2,A2scc)

  ! This subroutine extracts the computed value of the 
  ! spin-colour-correlated summed squared amplitude with "pow" power 
  ! of alpha_s, between particle with leg number "i1" and particle 
  ! with leg number "i2", for the process with process number "npr". 
  ! This routine must be called after 
  ! compute_spin_colour_correlation_rcl or 
  ! rescale_spin_colour_correlation_rcl for process with process 
  ! number "npr" for particles "i1" and "i2" with the  desired 
  ! polarization vestor.

  integer,  intent(in)  :: npr,pow,i1,i2
  real(dp), intent(out) :: A2scc

  integer               :: pr,i,legs,j1,j2,gspower

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of get_spin_colour_correlation_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*)         'ERROR: get_spin_colour_correlation_rcl called '
      write(nx,'(a,i3)') '        with undefined process index ',npr
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)

  if ((cpar(par(i1,pr)).ne.'g') .or. &
      (i1.le.0.or.i1.gt.legs)   .or. &
      (i2.le.0.or.i2.gt.legs)        &
     ) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: get_spin_colour_correlation_rcl called ', &
                         'with wrong indices'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  j1 = newleg(i1,pr)
  j2 = newleg(i2,pr)

  if (pow.lt.0.or.pow.gt.legs-2) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: get_spin_colour_correlation_rcl called ', &
                         'with wrong alphas power'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  gspower = 2*pow

  A2scc = matrix2scc(gspower,j1,j2,pr)

  end subroutine get_spin_colour_correlation_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine compute_spin_correlation_rcl (npr,p,j,v,A2sc, &
                                           momenta_check)

  ! This subroutine computes the summed squared amplitude in the 
  ! case where the polarization vector of one of the external photons 
  ! or gluons is given by the user.
  ! "p" are the external momenta of the process.
  ! "j" is an integer indicating the leg number for the photon or 
  ! gluon with the user-defined polarization vector.
  ! "v" is the user-defined polarization vector.
  ! "A2sc" is the spin-colour-correlated summed squared LO 
  ! amplitude.
  ! Summed squared amplitude means:
  ! - summed over all computed powers of g_s
  ! - summed over colour and spins for outgoing particles and averaged 
  !   for the incoming particles. 
  ! If some particles were defined as polarized, no sum/average is 
  ! performed on these particles. 
  ! "momenta_check" tells whether the phase-space point is good 
  ! (momenta_check=.true.) or bad (momenta_check=.false).

  integer,     intent(in)            :: npr,j
  real(dp),    intent(in)            :: p(0:,:)
  complex(dp), intent(in)            :: v(0:)
  real(dp),    intent(out), optional :: A2sc
  logical,     intent(out), optional :: momenta_check

  integer :: pr,i,legs

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of compute_spin_correlation_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*)         'ERROR: compute_spin_correlation_rcl called '
      write(nx,'(a,i3)') '        with undefined process index ',npr
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)

  if ( (cpar(par(j,pr)).ne.'A'.and.cpar(par(j,pr)).ne.'g') .or. &
       j.le.0.or.j.gt.legs                                      ) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: compute_spin_correlation_rcl called ', &
                         'with wrong index'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  if (size(p,2).ne.legs.or.size(p,1).ne.4) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: compute_spin_correlation_rcl called ', &
                         'with wrong momenta'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  call set_momenta (pr,p)
  if (present(momenta_check)) momenta_check = momcheck

  if (writeCor.ge.1) then
    call print_process_and_momenta (pr)
    call print_rescaling
    call print_parameters_change
  endif

  call compute_amplitude (pr,'LO')

  call rescale_amplitude (pr,'LO')

  if (writeMat.ge.1) call print_amplitude (pr,'LO')

  call compute_squared_amplitude_sc (pr,j,v)

  if (writeCor.ge.1) call print_squared_amplitude_sc (pr,j,v)

  if (present(A2sc)) A2sc = sum(matrix2sc(0:gs2Tot(0,pr),pr))

  end subroutine compute_spin_correlation_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine rescale_spin_correlation_rcl (npr,j,v,A2sc)

  ! This subroutine adjusts the results calculated by 
  ! compute_spin_correlation_rcl for a new value of alpha_s, 
  ! rescaling the structure-dressed helicity amplitudes and 
  ! recomputing the spin-correlated summed squared amplitude for 
  ! the process with process number "npr". 
  ! "j", "v" and "A2sc" are the same has for 
  ! compute_spin_correlation_rcl.

  integer,     intent(in)            :: npr,j
  complex(dp), intent(in)            :: v(0:)
  real(dp),    intent(out), optional :: A2sc

  integer :: pr,i,legs

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of rescale_spin_correlation_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*)         'ERROR: rescale_spin_correlation_rcl called '
      write(nx,'(a,i3)') '        with undefined process index ',npr
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)

  if ( (cpar(par(j,pr)).ne.'A'.and.cpar(par(j,pr)).ne.'g') .or. &
       j.le.0.or.j.gt.legs                                      ) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: rescale_spin_correlation_rcl called ', &
                         'with wrong index'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  if (writeCor.ge.1) call print_rescaling
  if (writeCor.ge.1) call print_parameters_change

  call rescale_amplitude (pr,'LO')

  if (writeMat.ge.1) call print_amplitude (pr,'LO')

  call compute_squared_amplitude_sc (pr,j,v)

  if (writeCor.ge.1) call print_squared_amplitude_sc (pr,j,v)

  if (present(A2sc)) A2sc = sum(matrix2sc(0:gs2Tot(0,pr),pr))

  end subroutine rescale_spin_correlation_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine get_spin_correlation_rcl (npr,pow,A2sc)

  ! This subroutine extracts the computed value of the 
  ! spin-correlated summed squared amplitude with "pow" power 
  ! of alpha_s for the process with process number "npr".
  ! This routine must be called after compute_spin_correlation_rcl 
  ! or rescale_spin_correlation_rcl for process with process number 
  ! "npr" with the desired polarization vestor.

  integer,  intent(in)  :: npr,pow
  real(dp), intent(out) :: A2sc

  integer :: pr,i,legs,gspower

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of get_spin_correlation_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,'(a,i3)') ' ERROR: get_spin_correlation_rcl called '// &
                                 'with undefined process index ',npr
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)

  if (pow.lt.0.or.pow.gt.legs-2) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: get_spin_correlation_rcl called ', &
                         'with wrong alphas power'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  gspower = 2*pow

  A2sc = matrix2sc(gspower,pr)

  end subroutine get_spin_correlation_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine get_momenta_rcl (npr,p)

  ! This subroutine extracts the stored momenta of the process with 
  ! process number "npr"

  ! This routine must be called after compute_process_rcl, 
  ! compute_colour_correlation_rcl, 
  ! compute_all_colour_correlations_rcl, 
  ! compute_spin_colour_correlation_rcl or 
  ! compute_spin_correlation_rcl for process with process number 
  ! "npr".

  integer,  intent(in)  :: npr
  real(dp), intent(out) :: p(0:,:)

  integer :: pr,i,legs

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of get_momenta_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,'(a,i3)') ' ERROR: get_momenta_rcl called '// &
                                 'with undefined process index ',npr
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)

  if (size(p,2).ne.legs.or.size(p,1).ne.4) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: get_momenta_rcl called with wrong momenta'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif
  p(0:3,1:legs) = momenta(0:3,1:legs)

  end subroutine get_momenta_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine set_TIs_required_accuracy_rcl (acc)

  ! This subroutine sets the required accuracy for TIs to the value
  ! "acc" in collier. 

  real(dp), intent(in) :: acc

  call SetReqAcc_cll  (acc)

  end subroutine set_TIs_required_accuracy_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine get_TIs_required_accuracy_rcl (acc)

  ! This subroutine extracts the value of the required accuracy
  ! for TIs from collier and returns it as value of the output 
  ! variable "acc". 

  real(dp), intent(out) :: acc

  real(dp) :: cllreqacc

  call GetReqAcc_cll (cllreqacc)

  acc = cllreqacc

  end subroutine get_TIs_required_accuracy_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine set_TIs_critical_accuracy_rcl (acc)

  ! This subroutine sets the critical accuracy for TIs to the value
  ! "acc" in collier. 

  real(dp), intent(in) :: acc

  call SetCritAcc_cll  (acc)

  end subroutine set_TIs_critical_accuracy_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine get_TIs_critical_accuracy_rcl (acc)

  ! This subroutine extracts the value of the critical accuracy
  ! for TIs from collier and returns it as value of the
  ! output variable "acc". 

  real(dp), intent(out) :: acc

  real(dp) :: cllcritacc

  call GetCritAcc_cll (cllcritacc)

  acc = cllcritacc

  end subroutine get_TIs_critical_accuracy_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine get_TIs_accuracy_flag_rcl (flag)

  ! This subroutine extracts the value of the accuracy flag for TIs 
  ! from collier. The output variable "flag" returns global 
  ! information on the accuracy of the TIs evaluated in the last call 
  ! of compute_process_rcl:
  ! - "flag = 0": 
  !   For all TIs, the accuracy is estimated to be better than the 
  !   required value.
  ! - "flag = -1": 
  !   For at least one TI, the accuracy is estimated to be worse than 
  !   the required value, but for all TIs, the accuracy is estimated 
  !   to be better than the critical value.
  ! - "flag = -2": 
  !   For at least one TI, the accuracy is estimated to be worse than 
  !   the critical values. 
  ! The value of variable "flag" is determined based on internal
  ! uncertainty estimations performed by collier. 

  integer, intent(out) :: flag

  integer  :: cllaccflag

  call GetAccFlag_cll (cllaccflag)

  flag = cllaccflag

  end subroutine get_TIs_accuracy_flag_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  end module process_computation_rcl

!#####################################################################














