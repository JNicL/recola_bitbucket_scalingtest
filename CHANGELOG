#==============================================================================#
#                              RECOLA Version 1.2                              #
#==============================================================================#

Notable changes with respect to version 1.1


New C++ interface:

- C++ header file include/recola.h
- Featuring all essential methods available in Recola
- C++ demo files demos/cdemo*_rcl.cpp exemplifying usage

Modified subroutines:

- (internal) compute_amplitude: 
  Numerically stable computation of invariants (p_i+p_j+...)^2

Compatibility:
- loop_functions_rcl: fix complex arguments in loop functions for compatibility
                      with ifort 16.0.2;

#==============================================================================#
#                              RECOLA Version 1.1                              #
#==============================================================================#

Notable changes with respect to version 1.0


New input parameters:

- dynamic_settings:
    Regulates which input parameters can be set dynamically.

- momenta_correction:
    Steers the intrinsic correction of external momenta p(0:3,1:), if they do 
    not fulfill the mass-shell condition or four-momentum conservation.


New subroutines: 

- set_mass_reg_soft_rcl:
    Sets the mass regulator for soft singularities.

- set_alphas_masses_rcl:
    Allows to define quark masses (different from the pole masses) exclusively 
    used for alpha_s renormalization..

- set_dynamic_settings:
    Sets the variable dynamic_settings.

- set_momenta_correction_rcl:
    Sets the variable momenta_correction.

- split_collier_cache_rcl:
    Allows the user to split the collier-cache in smaller parts. 

- get_colour_configurations_rcl:
    Allows the user to extract all colour configurations of the process.

- get_helicity_configurations_rcl:
    Allows the user to extract all helicity configurations of the process.


Modified subroutines:

- compute_process_rcl
- compute_colour_correlation_rcl
- compute_all_colour_correlations_rcl
- compute_spin_correlation_rcl
- compute_spin_colour_correlation_rcl: 
    Additional optional output argument called "momenta_check", which 
    tells the user whether the phase-space point is
    physical (momenta_check=T) or not (momenta_check=F).

- use_gfermi_scheme_rcl:
    Additional optional input argument "a".


Bug fixes:

Corrected bugs in:
- momenta correction;
- rescaling of the amplitude;
- set_alphas_rcl;
- warning messages concerning optional arguments;
- get_amplitude_rcl;
- printing of amplitudes;
- the R2 vertex for 'A tau- tau+';
- the recursion procedure (due to colour optimization).
